<?php

namespace TweedeGolf\MainBundle\DataFixtures\Faker\Provider;

use Faker\Generator;
use Faker\Provider\Base;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadedImage extends Base
{
    private $container;

    public function __construct(Generator $generator, ContainerInterface $container)
    {
        parent::__construct($generator);
        $this->container = $container;
    }

    public function uploadedImage($width, $height, $category = null)
    {
        $dir = $this->container->getParameter('kernel.cache_dir');
        $path = $this->generator->image($dir, $width, $height, $category);
        $name = 'image-' . $this->generator->randomNumber() . '.jpg';

        return new UploadedFile($path, $name, 'image/jpeg', filesize($path), UPLOAD_ERR_OK, true);
    }

    public function uploadedImageFromDir($directory)
    {
        $files = $this->getDirectoryListing($directory);
        $images = [];
        foreach ($files as $file) {
            if (in_array($this->getFileMimeType($file), ['image/jpeg', 'image/gif', 'image/png'])) {
                $images[] = $file;
            }
        }

        $file = static::randomElement($images);
        $mime = $this->getFileMimeType($file);
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $name = 'image-' . $this->generator->randomNumber() . ($ext ? '.' . $ext : '');

        return new UploadedFile($file, $name, $mime, filesize($file), UPLOAD_ERR_OK, true);
    }

    public function uploadedFileFromDir($directory)
    {
        if (!is_dir($directory)) {
            throw new \InvalidArgumentException(sprintf(
                'Source directory %s does not exist or is not a directory.',
                $directory
            ));
        }

        $file = static::randomElement($this->getDirectoryListing($directory));
        $mime = $this->getFileMimeType($file);
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $name = 'file-' . $this->generator->randomNumber() . ($ext ? '.' . $ext : '');

        return new UploadedFile($file, $name, $mime, filesize($file), UPLOAD_ERR_OK, true);
    }

    private function getDirectoryListing($directory)
    {
        return array_values(array_diff(scandir($directory), array('.', '..')));
    }

    private function getFileMimeType($path)
    {
        if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $path);
            finfo_close($finfo);

            return $mime;
        } elseif (function_exists('mime_content_type')) {
            return mime_content_type($path);
        } elseif (!stristr(ini_get('disable_functions'), 'shell_exec')) {
            $file = escapeshellarg($path);
            $mime = shell_exec('file -bi ' . $file);

            return $mime;
        } else {
            return false;
        }
    }
}
